---
layout: article
title: Submit posts
date: 2016-02-08 03:46:23 +07:00
permalink: /submit.html
modified:
excerpt:
image:
  feature:
  teaser:
  thumb: logo.png
ads: false
---

Submit your post are available now but follow step bellow.
all use command line svn and git in next time.
need Ruby Subversion and git for do this.

Get the code source

> Sign in or Sign up for Gitlab acoounts
> Fork it and merge request.
> Local source [svn.nulldo.xyz](http://svn.nulldo.xyz)

{% highlight shell %}
git clone https://gitlab.com/nulldoxyz/nulldoxyz.git # refer username
cd nulldoxyz
bundle install
{% endhighlight %}

Create new post

{% highlight shell %}
bundle exec octopress new post "My New Post"
{% endhighlight %}

Result ```_posts/YYYY-MM-DD-my-new-post.md```

{% highlight yaml %}
---
layout: article
title: My New Post
modified: 2016-02-05 08:21:55 +07:00
categories: php
excerpt: My New Post
tags: [laravel]
image:
  feature:
  teaser: Booster-Traffic-Exchange-System.png
  thumb: Booster-Traffic-Exchange-System.png
date: 2016-02-05 08:21:55 +07:00
comments: true
---
{% endhighlight %}

Edit and fill all think in ```_posts/YYYY-MM-DD-my-new-post.md```
Note: ```teaser:``` is for thumnail post in index and ```thumb:``` is for openGrap Twitter.

Push your change

{% highlight shell %}
git add --all
git commit -am "My New Post"
git push
{% endhighlight %}

For seconds Merge request step before push

{% highlight shell %}
cd nulldoxyz
git remote add upstream https://gitlab.com/nulldoxyz/nulldoxyz.git
git fetch upstream
git merge upstream/master
{% endhighlight %}

wait midnight cron will auto publish posts.